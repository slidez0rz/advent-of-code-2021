file = open("input.txt", "r");
x = 0
y = 0
aim = 0
while line := file.readline():
    command, amount = line.split()
    if command == "up":
        aim -= int(amount)
    elif command == "down":
        aim += int(amount)
    else:
        x += int(amount)
        y += aim * int(amount)
file.close()
print(x * y)
