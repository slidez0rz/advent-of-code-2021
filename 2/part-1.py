file = open("input.txt", "r");
x = 0
y = 0
while line := file.readline():
    command, amount = line.split()
    if command == "up":
        y -= int(amount)
    elif command == "down":
        y += int(amount)
    else:
        x += int(amount)
file.close()
print(x * y)
