file = open("input.txt", "r");
lastSum = None
values = []
increases = 0
while line := file.readline():
    currentValue = int(line)
    values.append(currentValue)
    if len(values) == 3:
        currentSum = sum(values)
    else:
        continue
    if lastSum is not None and currentSum > lastSum:
        increases += 1
    lastSum = currentSum
    values.pop(0)
print(increases)
