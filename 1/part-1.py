file = open("input.txt", "r");
lastValue = None
increases = 0
while line := file.readline():
    currentValue = int(line)
    if lastValue is not None and currentValue > lastValue:
        increases += 1
    lastValue = currentValue
print(increases)
