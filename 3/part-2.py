file = open("input.txt", "r");
fileData = []
while line := file.readline():
    bits = list(line)
    if bits[-1] == "\n":
        bits.pop()
    fileData.append(list(map(int, bits)))
oxygenNumbers = fileData.copy()
co2Numbers = fileData.copy()
oxygenNumber = None
co2Number = None
for x in range(0, len(oxygenNumbers[0])):
    bits = [0, 0]
    for number in oxygenNumbers:
        bit = int(number[x])
        bits[bit] += 1
    if bits[0] > bits[1]:
        oxygenNumbers = list(filter(lambda y: y[x] == 0, oxygenNumbers))
    else:
        oxygenNumbers = list(filter(lambda y: y[x] == 1, oxygenNumbers))
    if len(oxygenNumbers) == 1:
        oxygenNumber = ''.join(map(str, oxygenNumbers.pop()))
        break
for x in range(0, len(co2Numbers[0])):
    bits = [0, 0]
    for number in co2Numbers:
        bit = int(number[x])
        bits[bit] += 1
    if bits[0] > bits[1]:
        co2Numbers = list(filter(lambda y: y[x] == 1, co2Numbers))
    else:
        co2Numbers = list(filter(lambda y: y[x] == 0, co2Numbers))
    if len(co2Numbers) == 1:
        co2Number = ''.join(map(str, co2Numbers.pop()))
        break
file.close()
print(int(oxygenNumber, 2) * int(co2Number, 2))
