file = open("input.txt", "r");
bitmap = []
gamma = []
epsilon = []
while line := file.readline():
    bits = list(line)
    for index, bit in enumerate(bits):
        if bit == "\n":
            continue
        if index >= len(bitmap):
            bitmap.append([0, 0])
        bitmap[index][int(bit)] += 1
for bits in bitmap:
    if (bits[0] < bits[1]):
        gamma.append(1)
        epsilon.append(0)
    else:
        gamma.append(0)
        epsilon.append(1)
gamma = ''.join(map(str, gamma))
epsilon = ''.join(map(str, epsilon))
file.close()
print(int(gamma, 2) * int(epsilon, 2))
