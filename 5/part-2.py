file = open("input.txt", "r");
activePoints = {}
while line := file.readline():
    points = line.split(' -> ')
    point1 = list(map(int, points[0].split(',')))
    point2 = list(map(int, points[1].split(',')))
    x = point1[0]
    y = point1[1]
    doBreak = False
    while True:
        if x == point2[0] and y == point2[1]:
            doBreak = True
        coord = str(x) + "," + str(y)
        if coord in activePoints:
            activePoints[coord] += 1
        else:
            activePoints[coord] = 1
        if point1[0] > point2[0]:
            x -= 1
        elif point1[0] < point2[0]:
            x += 1
        if point1[1] > point2[1]:
            y -= 1
        elif point1[1] < point2[1]:
            y += 1
        if doBreak:
            break
file.close()
print(len(list(filter(lambda x: x[1] >= 2, activePoints.items()))))