file = open("input.txt", "r");
activePoints = {}
while line := file.readline():
    points = line.split(' -> ')
    point1 = list(map(int, points[0].split(',')))
    point2 = list(map(int, points[1].split(',')))
    if (point1[0] == point2[0]):
        x = point1[0]
        y1 = point1[1]
        y2 = point2[1]
        if y1 > y2:
            y1 = point2[1]
            y2 = point1[1]
        for y in range(y1, y2 + 1):
            coord = str(x) + "," + str(y)
            if coord in activePoints:
                activePoints[coord] += 1
            else:
                activePoints[coord] = 1
    elif (point1[1] == point2[1]):
        y = point1[1]
        x1 = point1[0]
        x2 = point2[0]
        if x1 > x2:
            x1 = point2[0]
            x2 = point1[0]
        for x in range(x1, x2 + 1):
            coord = str(x) + "," + str(y)
            if coord in activePoints:
                activePoints[coord] += 1
            else:
                activePoints[coord] = 1
file.close()
print(len(list(filter(lambda x: x[1] >= 2, activePoints.items()))))