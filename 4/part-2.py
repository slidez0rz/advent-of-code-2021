numbers = []
boards = []
file = open("input.txt", "r");
while line := file.readline():
    if len(numbers) == 0:
        numbers = list(map(int, line.split(',')));
    elif line == "\n":
        boards.append([])
    else:
        row = list(map(lambda x: {"number": int(x), "found": False}, filter(lambda x: x != "", line.split(' '))));
        boards[len(boards) - 1].append(row)
file.close()
def isLineCompleted(line):
    return len(list(filter(lambda x: x["found"] == False, line))) == 0
def isBoardCompleted(board):
    for r, row in enumerate(board):
        if isLineCompleted(row):
            return True
    for i in range(0, len(board[0])):
        line = []
        for row in board:
            line.append(row[i])
        if isLineCompleted(line):
            return True
    return False
lastWinner = []
lastNumber = None
winners = []
for random in numbers:
    for b, board in enumerate(boards):
        if (b in winners):
            continue
        for r, row in enumerate(board):
            for n, number in enumerate(row):
                if number["number"] == random:
                    boards[b][r][n]["found"] = True
    for b, board in enumerate(boards):
        if isBoardCompleted(board):
            if (b not in winners):
                winners.append(b)
                lastNumber = random
                lastWinner = board.copy()
unmarkedNumbers = [];
for row in lastWinner:
    unmarkedNumbers.extend(list(map(lambda x: x["number"], filter(lambda x: x["found"] == False, row))))
print(sum(unmarkedNumbers) * lastNumber)