file = open("input.txt", "r")
data = file.read()
file.close()
data = list(map(int, data.split(",")))
fishMap = {}
for i in range(0, 9):
    fishMap[i] = 0;
for d in data:
    fishMap[d] += 1
for day in range(0, 256):
    newMap = {}
    for i in range(0, 9):
        newMap[i] = 0;
    for i, days in enumerate(fishMap):
        count = fishMap[days]
        if days == 0:
            newMap[8] += count
            newMap[6] += count
        else:
            newMap[days - 1] += count
    fishMap = newMap
print(sum(newMap.values()))