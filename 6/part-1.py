file = open("input.txt", "r")
data = file.read()
file.close()
data = list(map(int, data.split(",")))
for day in range(0, 80):
    for i in range(0, len(data)):
        if data[i] == 0:
            data.append(8)
            data[i] = 6
        else:
            data[i] -= 1
print(len(data))